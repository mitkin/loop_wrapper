What is loop_wrapper?
===================
loop_wrapper will run a program or script for every date in a given range.

Examples of use
===============
loop_wrapper 20130105 20130213 doer {d:%Y%m%d}

