"""
Looper uses datetime or daterange to compute
list of dates

"""


import datetime as dt

def daterange(start, end, reverse=False, method='default'):
    """
    Return a list of days between start and end, inclusive.

    """
    if start > end:
        raise ValueError('Start date must be before end date.')

    if method is 'default' or 'datetime':
        dates = get_dates_datetime(start, end)
    elif method is 'dateutil':
        dates = get_dates_dateutil(start, end)
    else:
        raise Exception('Unknown daterange method "{0}"'.format(method))

    if reverse:
        dates = dates[::-1]
    return dates

def get_dates_datetime(start, end):
    """
    Get dates list using datetime only

    Args:
        start (datetime.datetime): first timestamp
        end (datetime.datetime): last timestamp
    Returns:
        dates (list): list of datetime timestamps
    """
    dates = [start + dt.timedelta(days=x) for x in range((end - start).days + 1)]

    return dates


def get_dates_dateutil(start, end):
    """
    Use dateutil.rrule to get date range

    Args:
        start (datetime): start timestamp
        end (datetime): end timestamp

    Returns:
        dates (list): list of datetime objects

    """
    from dateutil.rrule import rrule, DAILY

    dates = rrule(DAILY, dtstart=start).between(start, end, inc=True)

    return dates
