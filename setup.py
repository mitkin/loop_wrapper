# Today setuptools is de-facto the  packaging library for python
# Next version is probably going to be distutils2 and
# will be included in the standard library of Python3 as "packaging"
from setuptools import setup, find_packages

setup(
    name='loop_wrapper',
    # semantic versioning would be a good idea!
    version="1.0.1",

    # short package description
    description='Loop over timerange and \
              provide third party programs with a timestamp argument',

    # long package description
    long_description = open('README.md', 'r').read(),

    # project url
    url='loop_wrapper.example.com',

    # main author
    author='Thomas Lavergne',
    author_email='loop_wrapper_devs@email.com',

    # add contributors and maintainers if you like
    maintainers=['Cristina Luis', 'Mikhail Itkin'],

    # what's the target audience for this package?
      classifiers=[
          'Intended Audience :: Developers',
          'Topic :: Software Development'],
    # tags
    keywords='cli timedate',

    # what packages/modules should be included?
    # hint: every folder that has __init__.py
    # but you can exclude some
    packages=find_packages(exclude='tests'),

    # can also include files listed in MANIFEST.in
    include_package_data = True,

    # here are the executables
    scripts=['bin/loop_wrapper'],

    # package minimal requirements
    # for the complete environment setup use requirements.txt
    install_requires=['dateutil']
)
